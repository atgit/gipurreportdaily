﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;

namespace GipurReportDaily
{
    public partial class Form1 : Form
    {
        StringBuilder sb;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PressSchedule();

            if (File.Exists(@"T:\Application\C#\GipurReportDaily\Log.txt"))
            {
                sb = new StringBuilder();
                sb.Append("Completed successfully at -  " + DateTime.Now);

                // flush every 20 seconds as you do it
                File.AppendAllText(@"T:\Application\C#\GipurReportDaily\" + "Log.txt", sb.ToString() + Environment.NewLine);
                sb.Clear();
            }

            Close();
        }

        public void PressSchedule()
        {
            // יצירת קובץ אקסל
            // Add reference -> com -> Microsoft Excel 12.0 Object Library            
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;


            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            string StrSql = "";
            string mach = "";
            string shift = "";
            double val = 0;
            DateTime Latest;

            // שליפת קובץ  0-1
            xlApp = new Excel.Application();
            if (File.Exists(@"T:\Application\C#\0-1\01.xlsm"))
            {
                File.Delete(@"T:\Application\C#\0-1\01.xlsm");
                File.Copy(@"T:\AA_TAPI\A_Boker\TUHNOT\TfusatMahbesimOtom.xlsm", @"T:\Application\C#\0-1\01.xlsm");
            }
            xlWorkBook = xlApp.Workbooks.Open(@"T:\\Application\\C#\\0-1\\01.xlsm", 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item("Main");
            try
            {
                // וידוא ריצה אחת ביום בלבד
                //DateTime lastModified = File.get(@"T:\Application\C#\0-1\01.xlsm");
                xlWorkSheet.Rows.ClearFormats();
                Excel.Range usedRange = xlWorkSheet.UsedRange;
                // הרצת התוכנית ליצירת הקובץ
                StrSql = @"CALL BPCSOALI.SF@445C1('N','Y','N') ";
                dbs.executeInsertQuery(StrSql);

                StrSql = @"SELECT DATE FROM BPCSFALI.SF@445F FETCH FIRST ROW ONLY ";
                DT = dbs.executeSelectQueryNoParam(StrSql);
                DateTime ExcelTime = DateTime.FromOADate(xlWorkSheet.Cells[2, 2].Value2); // השמת תאריך דיפולטיבי אבל שונה מתאריך זמן השני
                shift = xlWorkSheet.Cells[2, 3].Value2.ToString();
                DateTime AS400Time = DateTime.ParseExact(DT.Rows[0]["DATE"].ToString(), "yyMMdd", CultureInfo.InvariantCulture);
                // מחיקת הנתונים מהקובץ
                Latest = DeleteRows(AS400Time, ExcelTime, shift);

                double T = 2d;
                T = AS400Time > ExcelTime ? 1 : (ExcelTime - AS400Time).TotalDays * 3; // נומרטור שיעזור לנו לקבוע מאיזה עמודה בקובץ להתחיל לקחת נתונים

                //StrSql = @"select DATE,min(rrn(BPCSFALI.SF@445F)),MACH,ITEM,sum(QTY01) as QTY01,sum(QTY02) AS QTY02,sum(QTY03) as QTY03,sum(QTY04) as QTY04,sum(QTY05) as QTY05,WEIT
                //            from BPCSFALI.SF@445F
                //            group by DATE,MACH,ITEM,WEIT
                //            order by min(rrn(BPCSFALI.SF@445F)) ";
                StrSql = @"select DATE,min(rrn(BPCSFALI.SF@445F)),MACH,ITEM,WEIT,";
                for (int i = 0; i <= 40; i++)
                {
                    StrSql += "sum(QTY" + (T + i).ToString("00") + ") as QTY" + (T + i).ToString("00") + ",";
                }
                StrSql = StrSql.Substring(0, StrSql.Length - 1);
                StrSql += @" from BPCSFALI.SF@445F
                            group by DATE,MACH,ITEM,WEIT
                            order by min(rrn(BPCSFALI.SF@445F)) ";
                //sum(QTY01) as QTY01,sum(QTY02) AS QTY02, sum(QTY03) as QTY03, sum(QTY04) as QTY04, sum(QTY05) as QTY05,
                //            from BPCSFALI.SF@445F
                //            group by DATE,MACH,ITEM,WEIT
                //            order by min(rrn(BPCSFALI.SF@445F)) ";
                DT = dbs.executeSelectQueryNoParam(StrSql);

                //if (lastModified < DateTime.Now)
                //{
                AS400Time = DateTime.ParseExact(DT.Rows[0]["DATE"].ToString(), "yyMMdd", CultureInfo.InvariantCulture);
                
                foreach (Excel.Range row in usedRange.Rows)
                {
                    mach = row.Cells[2, 1].Value2.ToString();
                    ExcelTime = DateTime.FromOADate(row.Cells[2, 2].Value2);
                    shift = row.Cells[2, 3].Value2.ToString();
                    val = double.Parse(row.Cells[2, 4].Value2.ToString());

                    //if (ExcelTime == AS400Time || ExcelTime == AS400Time.AddDays(1)) // נחפש את התאריכים של יום למחרת באקסל כדי להזין אותם
                    if (ExcelTime.Date >= Latest.Date && ExcelTime.Date < DateTime.Now.Date.AddDays(2))
                    {
                        if (!(ExcelTime == AS400Time && shift == "1"))
                        {
                            DataRow[] dr = DT.Select("MACH='" + mach + "'");
                            // int T = 2; // סמן שעוזר לרוץ על השדות הנכונים
                            T = AS400Time > ExcelTime ? (AS400Time - ExcelTime).TotalDays * 3 : (ExcelTime - AS400Time).TotalDays * 3; // נומרטור שיעזור לנו לקבוע מאיזה עמודה בקובץ להתחיל לקחת נתונים
                            foreach (DataRow R in dr)
                            {
                                if (double.Parse(R["QTY" + (T + int.Parse(shift) - 1).ToString("00")].ToString()) > 0.00)
                                {
                                    // נזין נתונים לקובץ ליום הבא
                                    StrSql = @"INSERT INTO BPCSFALI.SHMAQV values(" + ExcelTime.ToString("yyyyMMdd") + ", " +
                                             "0,0,'" + R["ITEM"] + "', '" + mach + "','" + shift + "',0," + R["QTY" + (T + int.Parse(shift) - 1).ToString("00")] + "," + R["WEIT"] + "," + val + ")";
                                    dbs.executeInsertQuery(StrSql);
                                }
                                //T++;
                            }
                        }
                        
                    }
                    // נוודא ריצה רק על היום
                    //if (AS400Time + shift == ExcelTime + "2" || AS400Time + shift == ExcelTime + "3" || AS400Time.AddDays(1) + shift == ExcelTime + "1" )
                    //if (AS400Time == ExcelTime && shift != "1") // נרוץ על היום ונעדכן את הקובץ. לא נעדכן את משמרת ראשונה כי אין לנו אותה בפיצוץ השיבוץ
                    //{
                    //DataRow[] dr = DT.Select("MACH='" + mach + "'");
                    ////  int T = 1; // סמן שעוזר לרוץ על השדות הנכונים
                    //foreach (DataRow R in dr)
                    //{
                    //    if (double.Parse(R["QTY0" + shift].ToString()) > 0.00)
                    //    {
                    //        // נעדכן את הקובץ
                    //        // נעדכן משמרות אחורה לאותו יום 
                    //        StrSql = @"UPDATE BPCSFALI.SHMAQV SET QPROD ='" + R["ITEM"] + "', QPLAN = " + R["QTY0" + shift] + ",QWGHT=" + R["WEIT"] + ",QMULT=" + val + " " +
                    //                  "WHERE QDATE =" + ExcelTime.ToString("yyyyMMdd") + " AND QMACH='" + mach + "' AND QSHFT=" + shift;
                    //        //StrSql = @"INSERT INTO BPCSFALI.SHMAQV values(" + ExcelTime.ToString("yyyyMMdd") + ", " +
                    //        //         "0,0,'" + R["ITEM"] + "', '" + mach + "','" + int.Parse(shift) + "',0," + R["QTY0" +  int.Parse(shift)] + "," + R["WEIT"] + "," + val + ")";
                    //        dbs.executeInsertQuery(StrSql);
                    //        //T++; // נקדם את הסמן בכל איטרציה
                    //    }
                    //}
                    //}
                    //else if (ExcelTime == AS400Time.AddDays(1)) // נחפש את התאריכים של יום למחרת באקסל כדי להזין אותם
                    //{
                    //    DataRow[] dr = DT.Select("MACH='" + mach + "'");
                    //    int T = 2; // סמן שעוזר לרוץ על השדות הנכונים
                    //    foreach (DataRow R in dr)
                    //    {
                    //        if (double.Parse(R["QTY0" + (T + int.Parse(shift))].ToString()) > 0.00)
                    //        {
                    //            // נזין נתונים לקובץ ליום הבא
                    //            StrSql = @"INSERT INTO BPCSFALI.SHMAQV values(" + ExcelTime.ToString("yyyyMMdd") + ", " +
                    //                     "0,0,'" + R["ITEM"] + "', '" + mach + "','" + shift + "',0," + R["QTY0" + (T + int.Parse(shift))] + "," + R["WEIT"] + "," + val + ")";
                    //            dbs.executeInsertQuery(StrSql);
                    //        }
                    //        //T++;
                    //    }
                    //}
                }
                // }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());

            }
            finally
            {
                xlWorkBook.Close(0);
                xlApp.Quit();
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
            }
        }

        /// <summary>
        /// Deletes the rows from SHMAQV for the latest earliest date and shift in 0-1 and SHMAL01 
        /// </summary>
        public DateTime DeleteRows(DateTime AS400, DateTime ExcelTime, string shift)
        {
            DBService dbs = new DBService();
            string StrSql = "";
            if (ExcelTime < AS400)
            {
                StrSql = @"DELETE FROM BPCSFALI.SHMAQV WHERE QDATE||QSHFT >= '" + AS400.ToString("yyyyMMdd") + shift + "' ";
                dbs.executeInsertQuery(StrSql);
                return AS400;
            }
            else
            {
                StrSql = @"DELETE FROM BPCSFALI.SHMAQV WHERE QDATE||QSHFT >= '" + ExcelTime.ToString("yyyyMMdd") + shift + "' ";
                dbs.executeInsertQuery(StrSql);
                return ExcelTime;
            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
